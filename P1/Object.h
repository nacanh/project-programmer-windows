#pragma once
#include<Windows.h>
#include <iostream>
using namespace std;
#define MAX_TEXT_LENGTH 100
#define MAX_OBJECT		200

class OBJECT {
public:
	int left, right, top, bottom, type;
	COLORREF color;
	LOGFONT font;
	WCHAR str[MAX_TEXT_LENGTH];
	virtual void Draw(HDC hdc) = 0;
};
class CLIPBOARD
{
public:
	int x1, y1, x2, y2;
	LOGFONT font;
	COLORREF color;
	int type;
	WCHAR str[MAX_TEXT_LENGTH];
};

class LINE : public OBJECT {
public:
	void Draw(HDC hdc);
};

class RECTANGLE : public OBJECT {
public:
	void Draw(HDC hdc);
};

class ELLIPSE : public OBJECT {
public:
	void Draw(HDC hdc);
};

class TEXTT : public OBJECT {
public:
	void Draw(HDC hdc);
};

class CHILD_WND_DATA {
public:
	HWND hWnd;
	COLORREF color;
	LOGFONT font;
	OBJECT *objects[MAX_OBJECT];
	int n;
};