#include"stdafx.h"
#include"Object.h"

void LINE::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 1.5, color);
	SelectObject(hdc, hPen);
	MoveToEx(hdc, left, top, NULL);
	LineTo(hdc, right, bottom);
	DeleteObject(hPen);
}

void RECTANGLE::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 1.5, color);
	SelectObject(hdc, hPen);
	Rectangle(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void ELLIPSE::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 1.5, color);
	SelectObject(hdc, hPen);
	Ellipse(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void TEXTT::Draw(HDC hdc) {
	HFONT hFont = CreateFontIndirect(&font);
	SelectObject(hdc, hFont);
	SetTextColor(hdc, color);
	TextOut(hdc, left, top, str, wcsnlen(str, MAX_TEXT_LENGTH));
	DeleteObject(hFont);
}