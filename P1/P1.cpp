﻿// P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "P1.h"
#include"Object.h"
#include<commdlg.h>
#include<CommCtrl.h>
#include<fstream>

#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32




// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hwndMDIClient = NULL;
COLORREF rgbCurrentColor = RGB(0, 0, 0);
HWND hFrameWnd;
WCHAR Drawtitle[MAX_LOADSTRING];
HWND  hToolBarWnd;
WPARAM wParamAll;
int currentDraw = ID_DRAW_LINE;
HWND editBox = NULL;
CLIPBOARD clipboard;
bool flag = false;
int index;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
void initFrameWindow(HWND);
LRESULT CALLBACK DrawChildWndProc(HWND, UINT, WPARAM, LPARAM);
void initChildWindow(HWND);
void onNewDrawWnd(HWND);
LRESULT CALLBACK MDICloseProc(HWND, LPARAM);
//toolbar
void doCreate_ToolBar(HWND hWnd);
void doToolBar_AddLine();
void doToolBar_AddRect();
void doToolBar_AddEl();
void doToolBar_AddText();
void doToolBar_AddSelect();
double getDistance(int x1, int y1, int x2, int y2);
bool onSelect(int x1, int y1, int x2, int y2, int &x3, int &y3, LPARAM lParam, int type);

void onChildPaint(HWND);
void onMouseMove(HWND, WPARAM, WPARAM,LPARAM, int, int, int&, int&);
void onLButtonUp(HWND, WPARAM, int, int, int &, int &);
void DialogSave(HWND);
void DialogOpen(HWND);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		if (!TranslateMDISysAccel(hwndMDIClient, &msg) &&
			!TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_P1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));
	RegisterClassExW(&wcex);

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = DrawChildWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"New Draw";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));
	RegisterClassExW(&wcex);

	return 1;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

	//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		initFrameWindow(hWnd);
		HMENU hDraw = GetSubMenu(GetMenu(hWnd), 2);
		CheckMenuItem(hDraw, 2, MF_CHECKED | MF_BYPOSITION);
		doCreate_ToolBar(hWnd);
		doToolBar_AddLine();
		doToolBar_AddRect();
		doToolBar_AddEl();
		doToolBar_AddText();
		doToolBar_AddSelect();
		return 0;
	}
	
    case WM_COMMAND:
        {
		wParamAll = wParam;
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case IDM_EXIT:
				DestroyWindow(hWnd);
			case ID_FILE_NEW:
				onNewDrawWnd(hWnd);
				break;
			case ID_FILE_OPEN:
				MessageBox(hWnd, L"Ban da cho Open !", L"Notice !", 0);
				DialogOpen(hWnd);
				break;
			case ID_FILE_SAVE:
				MessageBox(hWnd, L"Ban da cho Save !", L"Notice !", 0);
				DialogSave(hWnd);
				break;
			case ID_EDIT_CUT:
			{
				int IDRegist = RegisterClipboardFormat(L"Shape");
				HGLOBAL hgbMem = GlobalAlloc(GHND, sizeof(CLIPBOARD));

				HWND tmp = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
				CHILD_WND_DATA *data = (CHILD_WND_DATA*)GetWindowLongPtr(tmp, 0);

				CLIPBOARD* shape = (CLIPBOARD*)GlobalLock(hgbMem);
				shape->x1 = clipboard.x1;
				shape->y1 = clipboard.y1;
				shape->x2 = clipboard.x2;
				shape->y2 = clipboard.y2;
				shape->color = clipboard.color;
				shape->type = clipboard.type;
				if (clipboard.type == 4)
				{
					shape->font = clipboard.font;
					wcscpy_s(shape->str, wcslen(clipboard.str) + 1, clipboard.str);
				}
				GlobalUnlock(hgbMem);
				if (OpenClipboard(hWnd))
				{
					EmptyClipboard();
					SetClipboardData(IDRegist, hgbMem);
					CloseClipboard();
				}
			}
			case ID_EDIT_DELETE:
			{
				HWND tmp = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
				CHILD_WND_DATA *data = (CHILD_WND_DATA*)GetWindowLongPtr(tmp, 0);
				if (flag == true)
				{
					data->objects[index] = NULL;
					data->n--;
				}
				InvalidateRect(tmp, NULL, TRUE);
			}
			break;
			case ID_EDIT_COPY:
			{
				int IDRegist = RegisterClipboardFormat(L"Shape");
				HGLOBAL hgbMem = GlobalAlloc(GHND, sizeof(CLIPBOARD));

				CLIPBOARD* shape = (CLIPBOARD*)GlobalLock(hgbMem);
				shape->x1 = clipboard.x1;
				shape->y1 = clipboard.y1;
				shape->x2 = clipboard.x2;
				shape->y2 = clipboard.y2;
				shape->color = clipboard.color;
				shape->type = clipboard.type;
				if (clipboard.type == 4)
				{
					shape->font = clipboard.font;
					wcscpy_s(shape->str, wcslen(clipboard.str) + 1, clipboard.str);
				}
				GlobalUnlock(hgbMem);
				if (OpenClipboard(hWnd))
				{
					EmptyClipboard();
					SetClipboardData(IDRegist, hgbMem);
					CloseClipboard();
				}
				break;
			}
			case ID_EDIT_PASTE:
			{

				HWND tmp = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
				CHILD_WND_DATA *data = (CHILD_WND_DATA*)GetWindowLongPtr(tmp, 0);

				if (OpenClipboard(hWnd))
				{
					int IDRegist = RegisterClipboardFormat(L"Shape");
					HGLOBAL hgbMem = GlobalAlloc(GHND, sizeof(CLIPBOARD));
					HANDLE hData = GetClipboardData(IDRegist);

					if (hData != NULL)
					{
						CLIPBOARD *pData = (CLIPBOARD*)GlobalLock(hData);
						clipboard.color = pData->color;
						clipboard.type = pData->type;
						clipboard.x1 = pData->x1;
						clipboard.y1 = pData->y1;
						clipboard.x2 = pData->x2;
						clipboard.y2 = pData->y2;
						if (clipboard.type == 4)
						{
							clipboard.font = pData->font;
							wcscpy_s(clipboard.str, wcslen(pData->str) + 1, pData->str);
						}
						GlobalUnlock(hData);

						HDC hdc = GetDC(tmp);
						switch (clipboard.type)
						{
						case 1:
						{
							LINE * l = new LINE;
							l->left = clipboard.x1;
							l->top = clipboard.y1;
							l->right = clipboard.x2;
							l->bottom = clipboard.y2;
							l->color = clipboard.color;
							l->type = 1;
							data->objects[data->n] = (OBJECT*)l;
							data->n++;
						}
						break;
						case 2:
						{
							ELLIPSE * e = new ELLIPSE;
							e->left = clipboard.x1;
							e->top = clipboard.y1;
							e->right = clipboard.x2;
							e->bottom = clipboard.y2;
							e->color = clipboard.color;
							e->type = 2;
							data->objects[data->n] = (OBJECT*)e;
							data->n++;
						}
						break;
						case 3:
						{
							RECTANGLE *r = new RECTANGLE;
							r->left = clipboard.x1;
							r->top = clipboard.y1;
							r->right = clipboard.x2;
							r->bottom = clipboard.y2;
							r->color = clipboard.color;
							r->type = 3;
							data->objects[data->n] = (OBJECT*)r;
							data->n++;
						}
						break;
						case 4:
						{
							TEXTT* t = new TEXTT;

							wcscpy_s(t->str, wcslen(clipboard.str) + 1, clipboard.str);

							t->type = 4;
							int x1, y1, x2, y2;
							x1 = clipboard.x1;
							y1 = clipboard.y1;
							x2 = clipboard.x2;
							y2 = clipboard.y2;

							if (x1 < x2 && y2 > y1)
							{
								t->left = x1;
								t->top = y1;
								t->right = x2;
								t->bottom = y2;
							}
							else if (x2 < x1 && y1 > y2)
							{
								t->left = x2;
								t->top = y2;
								t->right = x1;
								t->bottom = y1;
							}
							else if (x1 > x2 && y2 > y1)
							{
								t->left = x2;
								t->top = y1;
								t->right = x1;
								t->bottom = y2;
							}
							else if (x1 < x2 && y1 > y2)
							{
								t->left = x1;
								t->top = y2;
								t->right = x2;
								t->bottom = y1;
							}
							t->color = clipboard.color;
							t->font = clipboard.font;
							data->objects[data->n] = (OBJECT*)t;
							data->n++;
						}
						break;
						}
						ReleaseDC(tmp, hdc);
						CloseClipboard();
					}
				}
				InvalidateRect(tmp, NULL, TRUE);
			}
			break;
			case ID_DRAW_COLOR:
			{
				HWND hColor =(HWND) SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
				CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hColor, 0);
				CHOOSECOLOR cc; 
				COLORREF acrCustClr[16]; 
				DWORD rgbCurrent = RGB(255, 0, 0); 
				ZeroMemory(&cc, sizeof(CHOOSECOLOR));
				cc.lStructSize = sizeof(CHOOSECOLOR);
				cc.hwndOwner = hWnd; 
				cc.lpCustColors = (LPDWORD)acrCustClr;
				cc.rgbResult = rgbCurrent;
				cc.Flags = CC_FULLOPEN | CC_RGBINIT;
				if (ChooseColor(&cc))
				{
					HBRUSH hbrush;
					hbrush = CreateSolidBrush(cc.rgbResult);
					rgbCurrent = cc.rgbResult;
					wndData->color = rgbCurrent;
				}
				break;
			}
			case ID_DRAW_FONT:
			{
				CHOOSEFONT cf; 
				LOGFONT lf;
				HFONT hfNew, hfOld;
				HWND hFont = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
				CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hFont, 0);

				ZeroMemory(&cf, sizeof(CHOOSEFONT));
				cf.lStructSize = sizeof(CHOOSEFONT);
				cf.hwndOwner = hWnd; 
				cf.lpLogFont = &lf;
				cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
				if (ChooseFont(&cf)) {
					HDC hdc = GetDC(hWnd);
					wndData->font = lf;
					wndData->color = cf.rgbColors;
					ReleaseDC(hWnd, hdc);	
				}
				break;
			}
			case ID_DRAW_ELLIPSE:
			{
				HMENU hMenu = GetSubMenu(GetMenu(hWnd), 1);
				CheckMenuItem(hMenu, 4, MF_CHECKED | MF_BYPOSITION);
				for (int i = 2; i < 6; i++) {
					if (i != 4) {
						CheckMenuItem(hMenu, i, MF_UNCHECKED | MF_BYPOSITION);
					}
				}
				currentDraw = ID_DRAW_ELLIPSE;
				break;
			}
			case ID_DRAW_LINE:
			{
				HMENU hMenu = GetSubMenu(GetMenu(hWnd), 1);
				CheckMenuItem(hMenu, 2, MF_CHECKED | MF_BYPOSITION);
				for (int i = 2; i < 6; i++) {
					if (i != 2) {
						CheckMenuItem(hMenu, i, MF_UNCHECKED | MF_BYPOSITION);
					}
				}
				currentDraw = ID_DRAW_LINE;
				break;
			}
			case ID_DRAW_RECTANGLE:
			{
				HMENU hMenu = GetSubMenu(GetMenu(hWnd), 1);
				CheckMenuItem(hMenu, 3, MF_CHECKED | MF_BYPOSITION);
				for (int i = 2; i < 6; i++) {
					if (i != 3) {
						CheckMenuItem(hMenu, i, MF_UNCHECKED | MF_BYPOSITION);
					}
				}
				currentDraw = ID_DRAW_RECTANGLE;
				break;
			}
			case ID_DRAW_TEXT:
			{
				HMENU hMenu = GetSubMenu(GetMenu(hWnd), 1);
				CheckMenuItem(hMenu, 5, MF_CHECKED | MF_BYPOSITION);
				for (int i = 2; i < 6; i++) {
					if (i != 5) {
						CheckMenuItem(hMenu, i, MF_UNCHECKED | MF_BYPOSITION);
					}
				}
				currentDraw = ID_DRAW_TEXT;
				break;
			}
			case ID_DRAW_SELECTOBJECT:
			{
				HMENU hMenu = GetSubMenu(GetMenu(hWnd), 1);
				CheckMenuItem(hMenu, 6, MF_CHECKED | MF_BYPOSITION);
				for (int i = 2; i < 6; i++) {
					if (i != 6) {
						CheckMenuItem(hMenu, i, MF_UNCHECKED | MF_BYPOSITION);
					}
				}
				currentDraw = ID_DRAW_SELECTOBJECT;
				break;
			}
			case ID_WINDOW_TIDE:
				SendMessage(hwndMDIClient, WM_MDITILE, 0, 0);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
				break;
			case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
				break;
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...

            EndPaint(hWnd, &ps);
        }
        break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_SIZE:
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 30, w, h, TRUE);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}

void initFrameWindow(HWND hWnd) {
	hFrameWnd = hWnd;
	CLIENTCREATESTRUCT css;
	css.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
	css.idFirstChild = 50001;
	hwndMDIClient = CreateWindow(L"MDICLIENT",
		(LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL,
		0, 0, 0, 0, hWnd,
		(HMENU)NULL, hInst, (LPVOID)&css);
	ShowWindow(hwndMDIClient, SW_SHOW);
}
LRESULT CALLBACK DrawChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int x1, x2, y1, y2;

	switch (message)
	{
	case WM_CREATE:
		initChildWindow(hWnd);
		return 0;
	case WM_DESTROY:
		break;
	case WM_CLOSE:
		break;
	case WM_SIZE:
		break;
	case WM_PAINT:
	{
		//InvalidateRect(hWnd, NULL, FALSE);
		onChildPaint(hWnd);
		break;
	}
	case WM_LBUTTONDOWN:

		if (editBox != NULL)
		{

			CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
			int size = GetWindowTextLength(editBox) + 1;
			WCHAR* str = new WCHAR[size];
			GetWindowText(editBox, str, size);
			TEXTT* t = new TEXTT;
			t->type = 4;
			if (x1 < x2 && y2 > y1)
			{
				t->left = x1;
				t->top = y1;
				t->right = x2;
				t->bottom = y2;
			}
			else if (x2 < x1 && y1 > y2)
			{
				t->left = x2;
				t->top = y2;
				t->right = x1;
				t->bottom = y1;
			}
			else if (x1 > x2 && y2 > y1)
			{
				t->left= x2;
				t->top = y1;
				t->right = x1;
				t->right = y2;
			}
			else if (x1 < x2 && y1 > y2)
			{
				t->left = x1;
				t->top = y2;
				t->right = x2;
				t->right = y1;
			}
			t->color = wndData->color;
			t->font = wndData->font;
			wcscpy_s(t->str, str);
			//MessageBox(hWnd, t->str, L"OK", MB_OK);
			wndData->objects[wndData->n] = (OBJECT*)t;
			wndData->n++;
			RECT r;
			GetClientRect(editBox, &r);
			MapWindowPoints(editBox, hWnd, (LPPOINT)&r, 2);
			DestroyWindow(editBox);
			editBox = NULL;
			HDC hdc = GetDC(hWnd);
			/*SetBkColor(hdc, TRANSPARENT);*/
			TextOut(hdc, r.left, r.top, str, size);
			ReleaseDC(hWnd, hdc);

		}
		x1 = x2 = LOWORD(lParam);
		y1 = y2 = HIWORD(lParam);
		static int x3, y3;
		if (LOWORD(wParamAll) == ID_DRAW_SELECTOBJECT) {
			CHILD_WND_DATA *data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
			x3 = LOWORD(lParam);
			y3 = HIWORD(lParam);
			for (int i = data->n - 1; i >= 0; i--)
			{
				if (onSelect(data->objects[i]->left, data->objects[i]->top, data->objects[i]->right, data->objects[i]->bottom, x3, y3, lParam, data->objects[i]->type))
				{
					WCHAR mess[20];
					RECT r;
					HDC hdc = GetDC(hWnd);

					clipboard.color = data->objects[i]->color;
					clipboard.x1 = data->objects[i]->left;
					clipboard.y1 = data->objects[i]->top;
					clipboard.x2 = data->objects[i]->right;
					clipboard.y2 = data->objects[i]->bottom;
					clipboard.type = data->objects[i]->type;
					if (data->objects[i]->type == 4)
					{
						wcscpy_s(clipboard.str, wcslen(data->objects[i]->str) + 1, data->objects[i]->str);
						clipboard.font = data->objects[i]->font;
					}
					index = i;
					flag = true;

					MessageBox(hWnd, L"Selected", L"Notice", MB_OK);
					DeleteObject(data->objects[i]);
					ReleaseDC(hWnd, hdc);
					break;
				}
			}
		}

		break;
	case WM_MOUSEMOVE:
		onMouseMove(hWnd, wParamAll,wParam, lParam, x1, y1, x2, y2);
		break;
	case WM_LBUTTONUP:
		onLButtonUp(hWnd, wParamAll, x1, y1, x2, y2);
		break;
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}
void onMouseMove(HWND hWnd, WPARAM wParamAll, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int& y2) {
	
		int currentDraw = LOWORD(wParamAll);
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		
		switch (currentDraw) {
		case ID_DRAW_LINE:
		{
			if (wParam & VK_LBUTTON == VK_LBUTTON)
			{
				
				HDC hdc = GetDC(hWnd);
				HPEN hPen = CreatePen(PS_SOLID, 1.5, wndData->color);
				SelectObject(hdc, hPen);
				SetROP2(hdc, R2_NOTXORPEN);
				MoveToEx(hdc, x1, y1, NULL);
				LineTo(hdc, x2, y2);

				//
				x2 = LOWORD(lParam);
				y2 = HIWORD(lParam);

				MoveToEx(hdc, x1, y1, NULL);
				LineTo(hdc, x2, y2);

				ReleaseDC(hWnd, hdc);
				DeleteObject(hPen);

			}
			break;
		}
		case ID_DRAW_ELLIPSE:
		{
			if (wParam & VK_LBUTTON == VK_LBUTTON)
			{

				HDC hdc = GetDC(hWnd);
				HPEN hPen = CreatePen(PS_SOLID, 1.5, wndData->color);
				SelectObject(hdc, hPen);
				SetROP2(hdc, R2_NOTXORPEN);
				Ellipse(hdc,x1,y1,x2,y2);

				x2 = LOWORD(lParam);
				y2 = HIWORD(lParam);

				Ellipse(hdc, x1, y1, x2, y2);
				ReleaseDC(hWnd, hdc);
				DeleteObject(hPen);

			}
			break;
		}
		case ID_DRAW_RECTANGLE:
		case ID_DRAW_TEXT:
			if (wParam & VK_LBUTTON == VK_LBUTTON)
			{
				HDC hdc = GetDC(hWnd);
				HPEN hPen = CreatePen(PS_SOLID, 1.5, wndData->color);
				SelectObject(hdc, hPen);
				SetROP2(hdc, R2_NOTXORPEN);
				Rectangle(hdc, x1, y1, x2, y2);

				//
				x2 = LOWORD(lParam);
				y2 = HIWORD(lParam);

				Rectangle(hdc, x1, y1, x2, y2);


				ReleaseDC(hWnd, hdc);
				DeleteObject(hPen);

			break;
		}
		}
}
void onLButtonUp(HWND hWnd, WPARAM wParam, int x1, int y1, int &x2, int &y2) {
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL) return;
	switch (currentDraw) {
		case ID_DRAW_LINE:
		{ 
			if (x1 != x2 && y1 != y2) {
				LINE *l = new LINE;
				l->type = 1;
				l->left = x1;
				l->top = y1;
				l->right = x2;
				l->bottom = y2;
				l->color = wndData->color;
				wndData->objects[wndData->n] = (OBJECT*)l;
				wndData->n++;
			}			
			break;
		}
		case ID_DRAW_ELLIPSE:
		{
			if (x1 != x2 && y1 != y2) {
				ELLIPSE *e = new ELLIPSE;
				e->type = 2;
				e->left = x1;
				e->top = y1;
				e->right = x2;
				e->bottom = y2;
				e->color = wndData->color;
				wndData->objects[wndData->n] = (OBJECT*)e;
				wndData->n++;
			}
			break;
		}
		case ID_DRAW_RECTANGLE:
		{
			if (x1 != x2 && y1 != y2) {
				RECTANGLE *r = new RECTANGLE;
				r->type = 3;
				r->left = x1;
				r->top = y1;
				r->right = x2;
				r->bottom = y2;
				r->color = wndData->color;
				wndData->objects[wndData->n] = (OBJECT*)r;
				wndData->n++;
			}
			break;
		}
		case ID_DRAW_TEXT:
		{
			if (x1 < x2 && y2 > y1) // dưới phải
			{
				editBox = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"", WS_CHILD | WS_VISIBLE,
					x1, y1, x2 - x1, y2 - y1, hWnd, nullptr, hInst, nullptr);
				HFONT hFont = CreateFont(wndData->font.lfHeight, wndData->font.lfWidth, wndData->font.lfEscapement, wndData->font.lfOrientation, wndData->font.lfWeight, wndData->font.lfItalic, wndData->font.lfUnderline,
					wndData->font.lfStrikeOut, wndData->font.lfCharSet, wndData->font.lfOutPrecision, wndData->font.lfClipPrecision, wndData->font.lfQuality, wndData->font.lfPitchAndFamily, wndData->font.lfFaceName);


				SendMessage(editBox, WM_SETFONT, WPARAM(hFont), TRUE);
			}
			else if (x2 < x1 && y1 > y2) // trên trái
			{
				editBox = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"", WS_CHILD | WS_VISIBLE,
					x2, y2, x1 - x2, y1 - y2, hWnd, nullptr, hInst, nullptr);
				HFONT hFont = CreateFont(wndData->font.lfHeight, wndData->font.lfWidth, wndData->font.lfEscapement, wndData->font.lfOrientation, wndData->font.lfWeight, wndData->font.lfItalic, wndData->font.lfUnderline,
					wndData->font.lfStrikeOut, wndData->font.lfCharSet, wndData->font.lfOutPrecision, wndData->font.lfClipPrecision, wndData->font.lfQuality, wndData->font.lfPitchAndFamily, wndData->font.lfFaceName);


				SendMessage(editBox, WM_SETFONT, WPARAM(hFont), TRUE);
			}
			else if (x1 > x2 && y2 > y1) // dưới trái
			{
				editBox = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"", WS_CHILD | WS_VISIBLE,
					x2, y1, x1 - x2, y2 - y1, hWnd, nullptr, hInst, nullptr);
				HFONT hFont = CreateFont(wndData->font.lfHeight, wndData->font.lfWidth, wndData->font.lfEscapement, wndData->font.lfOrientation, wndData->font.lfWeight, wndData->font.lfItalic, wndData->font.lfUnderline,
					wndData->font.lfStrikeOut, wndData->font.lfCharSet, wndData->font.lfOutPrecision, wndData->font.lfClipPrecision, wndData->font.lfQuality, wndData->font.lfPitchAndFamily, wndData->font.lfFaceName);


				SendMessage(editBox, WM_SETFONT, WPARAM(hFont), TRUE);
			}
			else if (x1 < x2 && y1 > y2) // trên phải
			{
				editBox = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"", WS_CHILD | WS_VISIBLE,
					x1, y2, x2 - x1, y1 - y2, hWnd, nullptr, hInst, nullptr);
				HFONT hFont = CreateFont(wndData->font.lfHeight, wndData->font.lfWidth, wndData->font.lfEscapement, wndData->font.lfOrientation, wndData->font.lfWeight, wndData->font.lfItalic, wndData->font.lfUnderline,
					wndData->font.lfStrikeOut, wndData->font.lfCharSet, wndData->font.lfOutPrecision, wndData->font.lfClipPrecision, wndData->font.lfQuality, wndData->font.lfPitchAndFamily, wndData->font.lfFaceName);
				SendMessage(editBox, WM_SETFONT, WPARAM(hFont), TRUE);
			}
			
			ShowWindow(editBox, SW_NORMAL);
			SetFocus(editBox);

		}
		
	}
}
void onChildPaint(HWND hWnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));

	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL) return;
	for (int i = 0; i < wndData->n; i++)
		wndData->objects[i]->Draw(hdc);
	EndPaint(hWnd, &ps);
}
void onNewDrawWnd(HWND hWnd) {
	MDICREATESTRUCT mdiCreate;
	ZeroMemory(&mdiCreate, sizeof(MDICREATESTRUCT));


	static int i = 1;
	wsprintf(Drawtitle, TEXT("Noname-%d.drw"), i);
	i++;

	mdiCreate.szClass = L"New Draw";
	mdiCreate.szTitle = Drawtitle;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)&mdiCreate);
}
void DialogSave(HWND hWnd)
{
	OPENFILENAME ofn;
	TCHAR  szFile[256] = L"Save As";
	TCHAR  szFilter[] = TEXT("Draw\0* .drw\0Text\0 *.txt; *.dat\0");

	HWND tmp = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
	CHILD_WND_DATA *data = (CHILD_WND_DATA*)GetWindowLongPtr(tmp, 0);

	//szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;  //  handle  của  window  cha
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;  //  chuỗi  tên  file  trả  về
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
	ofn.lpstrDefExt = L"txt";
	if (GetSaveFileName(&ofn))
	{
		fstream f;
		f.open(szFile, ios::out);
		f.write((char*)&data->n, sizeof(int));
		for (int i = 0; i < data->n; i++)
		{
			f.write((char*)&data->objects[i]->type, sizeof(int));
			f.write((char*)&data->objects[i]->left, sizeof(int));
			f.write((char*)&data->objects[i]->top, sizeof(int));

			if (data->objects[i]->type != 4)
			{
				f.write((char*)&data->objects[i]->right, sizeof(int));
				f.write((char*)&data->objects[i]->bottom, sizeof(int));
			}

			f.write((char*)&data->objects[i]->color, sizeof(COLORREF));

			if (data->objects[i]->type == 4)
			{
				f.write((char*)&data->font, sizeof(LOGFONT));
				int length = wcslen(data->objects[i]->str);
				f.write((char*)&length, sizeof(int));
				f.write((char*)data->objects[i]->str, sizeof(WCHAR)*(length + 1));
			}

		}

		f.close();
	}
}
void DialogOpen(HWND hWnd)
{
	COLORREF color;

	OPENFILENAME ofn;
	TCHAR  szFile[256];
	TCHAR  szFilter[] = TEXT("Draw\0* .drw\0Text\0 *.txt; *.dat\0");

	int i = 0;

	szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;  //  handle  của  window  cha
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;  //  chuỗi  tên  file  trả  về
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	GetOpenFileName(&ofn);

	onNewDrawWnd(hWnd);
	HWND tmp = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, NULL, NULL);
	CHILD_WND_DATA *data = (CHILD_WND_DATA*)GetWindowLongPtr(tmp, 0);

	int type;
	ifstream f;
	f.open(szFile, ios::in | ios::binary);
	int size;
	f.read((char*)&size, sizeof(int));
	for (int i = 0; i < size; i++)
	{
		f.read((char*)&type, sizeof(int));
		if (type == 1)
		{
			LINE *l = new LINE;
			l->type = type;
			f.read((char*)&l->left, sizeof(int));
			f.read((char*)&l->top, sizeof(int));
			f.read((char*)&l->right, sizeof(int));
			f.read((char*)&l->bottom, sizeof(int));
			f.read((char*)&l->color, sizeof(COLORREF));
			data->objects[data->n] = (LINE*)l;
			data->n++;
		}
		else if (type == 2)
		{
			ELLIPSE *e = new ELLIPSE;
			e->type = type;
			f.read((char*)&e->left, sizeof(int));
			f.read((char*)&e->top, sizeof(int));
			f.read((char*)&e->right, sizeof(int));
			f.read((char*)&e->bottom, sizeof(int));
			f.read((char*)&e->color, sizeof(COLORREF));
			data->objects[data->n] = (ELLIPSE*)e;
			data->n++;
		}
		else if (type == 3)
		{
			RECTANGLE *r = new RECTANGLE;
			r->type = type;
			f.read((char*)&r->left, sizeof(int));
			f.read((char*)&r->top, sizeof(int));
			f.read((char*)&r->right, sizeof(int));
			f.read((char*)&r->bottom, sizeof(int));
			f.read((char*)&r->color, sizeof(COLORREF));
			data->objects[data->n] = (RECTANGLE*)r;
			data->n++;
		}
		else if (type == 4)
		{
			TEXTT *t = new TEXTT;
			t->type = type;
			f.read((char*)&t->left, sizeof(int));
			f.read((char*)&t->top, sizeof(int));
			f.read((char*)&t->color, sizeof(COLORREF));
			f.read((char*)&t->font, sizeof(LOGFONT));
			int length;
			f.read((char*)&length, sizeof(int));
			f.read((char*)t->str, sizeof(WCHAR)*(length + 1));
			data->objects[data->n] = (TEXTT*)t;
			data->n++;
		}
	}
	f.close();
}

double getDistance(int x1,int y1,int x2,int y2)
{
	return (sqrt(1.0*(x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}

bool onSelect(int x1,int y1,int x2,int y2,int &x3,int &y3, LPARAM lParam, int type)
{
	x3 = LOWORD(lParam);
	y3 = HIWORD(lParam);
	switch (type)
	{
	case 1:
	{
		if (getDistance(x3, y3, x1, y1) + getDistance(x3, y3, x2, y2) >= getDistance(x1, y1, x2, y2) - 0.1 && getDistance(x3, y3, x1, y1) + getDistance(x3, y3, x2, y2) <= getDistance(x1, y1, x2, y2) + 0.1)
			return true;
	}
	break;
	case 2:
	case 3:
	case 4:
	{
		if (x1 < x2 && y1 < y2)
		{
			if ((x1 < x3 && x3 < x2) && (y1 < y3 && y3 < y2))
				return true;
		}
		if (x1 > x2 && y1 > y2)
		{
			if ((x1 > x3 && x3 > x2) && (y1 > y3 && y3 > y2))
				return true;
		}
		if (x1 > x2 && y1 < y2)
		{
			if ((x1 > x3 && x3 > x2) && (y1 < y3 && y3 < y2))
				return true;
		}
		if (x1 < x2 && y1 > y2)
		{
			if ((x1 < x3 && x3 < x2) && (y1 > y3 && y3 > y2))
				return true;
		}
	}
	break;
	}
	return false;
}
LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam) {
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hChildWnd, 0L);
	return 1;
}
void initChildWindow(HWND hWnd) {
	CHILD_WND_DATA *wndData;
	wndData = (CHILD_WND_DATA*)VirtualAlloc(NULL, sizeof(CHILD_WND_DATA),
		MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	wndData->hWnd = hWnd;
	wndData->color = RGB(0, 0, 0);
	ZeroMemory(&(wndData->font), sizeof(LOGFONT));
	wndData->font.lfHeight = 20;
	wcscpy_s(wndData->font.lfFaceName, LF_FACESIZE, L"Arial");
	wndData->n = 0;
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData) == 0)
		if (GetLastError() != 0) {
			int a = 0;
		}
}
void doCreate_ToolBar(HWND hWnd) {

	InitCommonControls();

	TBBUTTON tbButtons[] =
	{

		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0 , 0, TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ STD_COPY, ID_EDIT_COPY,	TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, 0, 0 },
		{ STD_CUT, ID_EDIT_CUT,	TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, 0, 0 },
		{ STD_PASTE, ID_EDIT_PASTE,	TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, 0, 0 },
		{ STD_DELETE, ID_EDIT_DELETE,	TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, 0, 0 }
	};
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));
}
void doToolBar_AddLine()
{
	// define new buttons
	TBBUTTON tbButtons[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[1].iBitmap += idx;


	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}
void doToolBar_AddRect() {
	// define new buttons
	TBBUTTON tbButtons[] =
	{

		{ 0, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP6 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[0].iBitmap += idx;


	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}
void doToolBar_AddEl() {
	// define new buttons
	TBBUTTON tbButtons[] =
	{
		{ 0, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
		
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP7 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[0].iBitmap += idx;


	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}
void doToolBar_AddText() {
	// define new buttons
	TBBUTTON tbButtons[] =
	{
		{ 0, ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP8 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[0].iBitmap += idx;


	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}
void doToolBar_AddSelect() {
	// define new buttons
	TBBUTTON tbButtons[] =
	{
		{ 0, ID_DRAW_SELECTOBJECT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP9 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[0].iBitmap += idx;


	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
}